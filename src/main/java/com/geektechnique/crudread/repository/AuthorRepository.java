package com.geektechnique.crudread.repository;

import java.util.List;

import com.geektechnique.crudread.domain.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long> {

	List<Author> findAllByOrderByLastNameAscFirstNameAsc();

}
